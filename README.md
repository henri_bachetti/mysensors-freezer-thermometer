# MYSENSORS FREEZER THERMOMETER

The purpose of this page is to explain step by step the realization of a freezer thermometer based on ARDUINO NANO, connected by radio to a DOMOTICZ server, using an NRF24L01 2.4GHZ module.

The board uses the following components :

 * an ARDUINO PRO MINI 3.3V 8MHz
 * an NRF24L01
 * a DS18B20 temperature sensor
 * 3 LEDs (green, orange, red)
 * some passive components
 * the board is powered by a MEANWELL IRM-01-5

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/05/un-thermometre-mysensors-pour-freezer.html

