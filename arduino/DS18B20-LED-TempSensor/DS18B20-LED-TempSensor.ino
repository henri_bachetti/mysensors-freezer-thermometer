
// Enable debug prints to serial monitor
//#define MY_DEBUG 

// Enable and select radio type attached
#define MY_RADIO_NRF24
//#define MY_RADIO_RFM69

#define MY_RF24_CE_PIN 7
#define MY_RF24_CS_PIN 8

#define GREEN_LED      6
#define ORANGE_LED     5
#define RED_LED        4

#include <SPI.h>
#include <MySensors.h>  
#include <DallasTemperature.h>
#include <OneWire.h>

#define COMPARE_TEMP 0 // Send temperature only if changed? 1 = Yes 0 = No

#define ONE_WIRE_BUS 3 // Pin where dallase sensor is connected 
#define MAX_ATTACHED_DS18B20 16
OneWire oneWire(ONE_WIRE_BUS); // Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
DallasTemperature sensors(&oneWire); // Pass the oneWire reference to Dallas Temperature. 
unsigned long SLEEP_TIME = 15L*60L*1000L;
int numSensors=0;
bool receivedConfig = false;
bool metric = true;
// Initialize temperature message
MyMessage tempMsg(0,V_TEMP);

#define CHILD_ID_TEMP         0

void before()
{
  Serial.print("MYSENSORS DS18B20 Temperature sensor: ");
  pinMode(RED_LED, OUTPUT);
  pinMode(ORANGE_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  digitalWrite(RED_LED, LOW);
  digitalWrite(ORANGE_LED, LOW);
  digitalWrite(GREEN_LED, LOW);
  // Startup up the OneWire library
  sensors.begin();
  Serial.println("OK");
}

void setup()  
{ 
  Serial.print("Setup");
  // requestTemperatures() will not block current thread
  sensors.setWaitForConversion(false);
  Serial.println(" OK");
}

void presentation() {
  Serial.print("Presentation");
  // Send the sketch version information to the gateway and Controller
  sendSketchInfo("Freezer Temperature Sensor", "1.1");

  // Fetch the number of attached temperature sensors  
  numSensors = sensors.getDeviceCount();

  // Present all sensors to controller
  present(CHILD_ID_TEMP, S_TEMP);
  Serial.println(" OK");
  digitalWrite(RED_LED, HIGH);
  digitalWrite(ORANGE_LED, HIGH);
  digitalWrite(GREEN_LED, HIGH);
}

void readDS18B20(void)
{
  static float lastTemperature;
  
  // Fetch temperatures from Dallas sensors
  sensors.requestTemperatures();

  // query conversion time and sleep until conversion completed
  int16_t conversionTime = sensors.millisToWaitForConversion(sensors.getResolution());
  // sleep() call can be replaced by wait() call if node need to process incoming messages (or if node is repeater)
  sleep(conversionTime);

  // Read temperatures and send them to controller 
  // Fetch and round temperature to one decimal
  float temperature = static_cast<float>(static_cast<int>((getControllerConfig().isMetric ? sensors.getTempCByIndex(0) : sensors.getTempFByIndex(0)) * 10.)) / 10.;

  // Only send data if temperature has changed and no error
  #if COMPARE_TEMP == 1
  if (lastTemperature != temperature && temperature != -127.00 && temperature != 85.00) {
  #else
  if (temperature != -127.00 && temperature != 85.00) {
  #endif
    // Send in the new temperature
    send(tempMsg.setSensor(CHILD_ID_TEMP).set(temperature, 1));
    // Save new temperatures for next compare
    lastTemperature = temperature;
    Serial.print("transmitted OK: ");
    Serial.print(temperature);
    Serial.println(" Celsius");
    digitalWrite(RED_LED, HIGH);
    digitalWrite(ORANGE_LED, HIGH);
    digitalWrite(GREEN_LED, HIGH);
    if (temperature < -18) {
      digitalWrite(GREEN_LED, LOW);
    }
    else if (temperature < -12) {
      digitalWrite(ORANGE_LED, LOW);
    }
    else {
      digitalWrite(RED_LED, LOW);
    }
  }
}

void loop()     
{
  readDS18B20();
  sleep(SLEEP_TIME);
}

